﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseFOFiles
{
    class Program
    {
        static void Main(string[] args)
        {

            //procitaj FO file u text
            string text;
            string linijaIsprekidana = " ------------ --------------- --- -------- -------------------------- ------- ----------------------- -------                        ";
            //string folderPath = @"C:\Users\klazarevic\Documents\NovaBanka\2017-Avg-dec\";
            string folderPath = @"C:\temp\FO\MC170904\";


            // C:\temp\FO\MC190315    C:\Users\klazarevic\Documents\NovaBanka\2016KL\  
            //procitaj file  string line = File.ReadLines(FileName).Skip(14).Take(1).First();
            //using (var streamReader = new StreamReader(@"c:\\temp\FO\F1218526.140", Encoding.UTF8))
            //{
            //    text = streamReader.ReadToEnd();
            //}

            //============================ procitaj sve fileove .140 u folderu
            Directory.EnumerateFiles(folderPath, "*.140", SearchOption.AllDirectories);

            foreach (string file in Directory.EnumerateFiles(folderPath, "*.140", SearchOption.AllDirectories))
            {
                string fileName = Path.GetFileName(file);
                //if (fileName== "F0876789.140")
                //{
                //    Console.Write("Stop Septembar!!!");
                //}
                string datumFilea = File.ReadLines(file).Skip(2).Take(1).First().Substring(62, 10);
                string contents = File.ReadAllText(file);
                ParsirajFile(contents, linijaIsprekidana, fileName, datumFilea);
            }


            //============================

            //ParsirajFile(text, linijaIsprekidana);

        }

        private static void ParsirajFile(string text, string linijaIsprekidana, string fileName, string datumFilea)
        {
            //podijeli izmedju ACKNOWLEDGEMENT
            string[] resultAcknowledgement;
            string[] resultNotification;

            // PODIJELI IZMEDJU ACKNOWLEDGEMENT
            resultAcknowledgement = text.Split(new string[] { "ACKNOWLEDGEMENT" }, StringSplitOptions.RemoveEmptyEntries);
            resultAcknowledgement = resultAcknowledgement.Skip(1).ToArray();
            resultAcknowledgement = resultAcknowledgement.Take(resultAcknowledgement.Count() - 1).ToArray();

            // PODIJELI IZMEDJU NOTIFICATION
            resultNotification = text.Split(new string[] { "NOTIFICATION" }, StringSplitOptions.RemoveEmptyEntries);
            resultNotification = resultNotification.Skip(1).ToArray();
            resultNotification = resultNotification.Take(resultNotification.Count() - 1).ToArray();

            // LOOP ACKNOWLEDGEMENT
            foreach (var item in resultAcknowledgement)
            {

                // podaci su izmedju dvije  -------------- ------------- ----------------
                string podaci = getBetween(item, linijaIsprekidana, linijaIsprekidana);

                if (!string.IsNullOrWhiteSpace(podaci))
                {

                    // linije izmedju dvije ------------ ------- ----------
                    string[] linije = podaci.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                    // svakoj liniji dodaj ACKNOWLEDGEMENT na pocetku linije
                    foreach (var linija in linije)
                    {
                        string linijaZaStampu = String.Format("ACKNOWLEDGEMENT {0} {1} {2} \n", datumFilea, fileName, linija);
                        File.AppendAllText("C:\\temp\\FO\\IzlazTest.txt", linijaZaStampu);
                    }


                }

            }

            // LOOP NOTIFICATION
            foreach (var item in resultNotification)
            {

                // provjeriti  koliko se puta pojavljuje linijaIsprekidana u item
                // broj pojavljivanja isprekidane linije linije

                int broj = CountStringOccurrences(item, linijaIsprekidana);

                if (broj > 2)
                {
                    // ako je broj linija 3 onda pronadji zadnju liniju koja pocinje 
                    string feeColl=" FEE COLL-CSG FEE COL ";
                    int redZadnjeFee = item.LastIndexOf(feeColl);
                    string posebniRed = item.Substring(item.LastIndexOf(feeColl), 133);

                    // linija za stampu
                    string linijaZaStampuFeeColl = String.Format("NOTIFICATIONXXX {0} {1} {2} \n", datumFilea, fileName, posebniRed);

                    //// ako je notification and " FEE COLL-CSG FEE COL CR ORIG            1                    0.30 DR 977-BAM                    0 CR    -     "
                    ////treba zamijeniti 0 CR sa iznos i DR u ovom slucaju 0.30 DR
                    //// zamijeniti od 90 - 102 sa podacima od 58-70

                    //string rezultantniRed = string.Empty;
                    if (posebniRed.Contains(" FEE COLL-CSG FEE COL CR ORIG"))
                    {
                        string replace = posebniRed.Substring(57, 20);
                        string rezultantniRed = posebniRed.Remove(89, 20).Insert(89, replace);
                        linijaZaStampuFeeColl = String.Format("NOTIFICATIONXXX {0} {1} {2} \n", datumFilea, fileName, rezultantniRed);
                    }
                    
                    File.AppendAllText("C:\\temp\\FO\\IzlazTest.txt", linijaZaStampuFeeColl);
                    Console.Write("stop!");
                }


                // podaci su izmedju dvije  -------------- ------------- ----------------
                string podaci = getBetween(item, linijaIsprekidana, linijaIsprekidana);

                if (!string.IsNullOrWhiteSpace(podaci))
                {
                    // linije izmedju dvije ------------ ------- ----------
                    string[] linije = podaci.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                    // svakoj liniji dodaj NOTIFICATION na pocetku linije
                    foreach (var linija in linije)
                    {
                        string linijaZaStampu = String.Format("NOTIFICATIONXXX {0} {1} {2} \n", datumFilea, fileName, linija);

                        // provjera   " FEE COLL-CSG FEE COL CR ORIG"
                        if (linija.Contains(" FEE COLL-CSG FEE COL CR ORIG"))
                        {
                            string replace = linija.Substring(57, 20);
                            string rezultantniRed = linija.Remove(89, 20).Insert(89, replace);
                            linijaZaStampu = String.Format("NOTIFICATIONXXX {0} {1} {2} \n", datumFilea, fileName, rezultantniRed);
                        }
                        // end      " FEE COLL-CSG FEE COL CR ORIG"

                        File.AppendAllText("C:\\temp\\FO\\IzlazTest.txt", linijaZaStampu);
                    }

                }

            }
        }

        //  
        // int count = source.Split('/').Length - 1;
        //
        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;

            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Count occurrences of strings.
        /// </summary>
        public static int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }
    }
}
